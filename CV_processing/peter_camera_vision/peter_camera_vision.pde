import processing.video.*;

import processing.video.*;
 
Capture video;
MovieMaker mm;
boolean captureMode = true;
 
 
void setup() {
  size(640, 600);
 
 
  //stuff drawing video image to screen
  video = new Capture(this, 640, 480, 30);
  
  //stuff for video capture
  mm = new MovieMaker(this, width, height, "data/sesson.ogg");
  
}

void movieEvent(Movie myMovie) {
  myMovie.read();
}

void draw() {
  println(frameRate);
  
  if (video.available()) {
    video.read();
    video.loadPixels();
  }
 
 
  //make movie frames if in movie mode
  if(captureMode == true)
  {
    loadPixels();
    mm.addFrame();
  }
  
}
 
 
 
void keyPressed()
{
  if (key == ' ') {
    // Finish the movie if space bar is pressed
    mm.finish();
    // Quit running the sketch once the file is written
    exit();
  }
  if (key == 'v')
  {
    captureMode = true;
  }
  if (key == 'c')
  {
    captureMode = true;
  }
  if(key == 'x')
  {
    captureMode = false;
  }
}
 
void mousePressed()
{
}
 
