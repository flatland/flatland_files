package cbdetect;
import java.util.HashMap;
import java.util.Map;

import processing.core.*;

public class MainTest extends PApplet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5506896276323393530L;
	int cRadius;
	Map<String,PVector> hm = new HashMap<String,PVector>();

	public void setup()
	{
		cRadius = 0;
		size(800,480);
		
		PVector a = new PVector(10,10);
		PVector b = new PVector(10,10);
		
		hm.put(a.toString(), a);
		hm.put(b.toString(), b);
		println(hm.toString());
		println(hm.size());
		
		
	}
	
	public void draw()
	{
		int top = -(cRadius+2)/2;
		int bot = (cRadius+2)/2;
		int left = -(cRadius+2)/2;
		int right = (cRadius+2)/2;
		
		for(int i = left; i <= right; i++)
		{
			point(i+300,top+300);
			point(i+300,bot+300);
		}
		for(int i = top; i <= bot; i++)
		{
			point(left+300,i+300);
			point(right+300,i+300);
		}
		cRadius++;
	}
	

}
